package pl.sda.javagda12.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> dane = new ArrayList<>(Arrays.asList("aa", "bg", "cww", "d", "e"));

//        List<String> filtrowane = dane.stream()
//                .filter(new Predicate<String>() {
//                    @Override
//                    public boolean test(String s) {
//                        return s.length() > 1;
//                    }
//                }).collect(Collectors.toList());

        List<String> filtrowane = dane.stream()
                .filter(s -> s.length() > 1)
                .collect(Collectors.toList());
        //---------------------------------------------------------

//        List<Osoba> listaOsob = dane.stream()
//                .map(new Function<String, Osoba>() {
//                    @Override
//                    public Osoba apply(String s) {
//                        return new Osoba(s);
//                    }
//                })
//                .collect(Collectors.toList());


        List<Osoba> listaOsob = dane.stream()
                .map(s -> new Osoba(s))
                .collect(Collectors.toList());


        dane.stream()
                .map(s -> new Osoba(s))
                .forEach(new Consumer<Osoba>() {
                    @Override
                    public void accept(Osoba osoba) {
                        System.out.println(osoba);
                    }
                });
        //---------------------------------------------------------

        listaOsob.forEach(osoba -> System.out.println(osoba));



    }
}
