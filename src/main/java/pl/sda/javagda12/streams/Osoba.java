package pl.sda.javagda12.streams;

import java.util.Random;

public class Osoba {
    private String imie;
    private int wiek;

    public Osoba(String imie) {
        this.imie = imie;
        this.wiek = new Random().nextInt(20) + 10;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                '}';
    }
}
