package pl.sda.javagda12.streams;

import java.lang.reflect.Array;
import java.util.*;

public class MainMapToInt {
    public static void main(String[] args) {
        List<Osoba> list = new ArrayList<>(Arrays.asList(
                new Osoba("Rafau"),
                new Osoba("Michau"),
                new Osoba("Paweu"),
                new Osoba("awdawdawdu")
        ));

        list.forEach(osoba -> System.out.println(osoba));

        OptionalDouble srednia = list.stream()
                .mapToInt(o -> o.getWiek())
                .average();
        if (srednia.isPresent()) {
            System.out.println(srednia);
        }
        ////----------------------------------------

        int suma = list.stream()
                .mapToInt(o -> o.getWiek())
                .sum();

        System.out.println(suma);
        //-------------------------------------------
        Optional<Osoba> osobaStreamu = list.stream()
                .filter(osoba -> osoba.getImie().equalsIgnoreCase("awdawdawdu"))
                .findFirst();

//        Optional<Osoba> najstarszy = list.stream()
//                .sorted(new Comparator<Osoba>() {
//                    @Override
//                    public int compare(Osoba o1, Osoba o2) {
//                        if (o1.getWiek() > o2.getWiek()) {
//                            return 1;
//                        } else if (o2.getWiek() > o1.getWiek()) {
//                            return -1;
//                        }
//                        return 0;
//                    }
//                }).findFirst();
        Optional<Osoba> najstarszy = list.stream()
                .sorted((o1, o2) -> {
                    if (o1.getWiek() > o2.getWiek()) {
                        return 1;
                    } else if (o2.getWiek() > o1.getWiek()) {
                        return -1;
                    }
                    return 0;
                }).findFirst();
    }
}
