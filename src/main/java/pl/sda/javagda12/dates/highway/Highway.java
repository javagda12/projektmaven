package pl.sda.javagda12.dates.highway;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class Highway {
    private Map<String, LocalDateTime> rejestr = new HashMap<>();

    public void dodajDoRejestru(String nrRejestracyjny) {
        rejestr.put(nrRejestracyjny, LocalDateTime.now());
    }

    public void obliczKoszt(String nrRejestracyjny) {
        if (!rejestr.containsKey(nrRejestracyjny)) {
            return;
        }

        LocalDateTime czasWjazdu = rejestr.get(nrRejestracyjny);
        LocalDateTime czasWyjazdu = LocalDateTime.now();

        Duration roznicaCzasu = Duration.between(czasWjazdu, czasWyjazdu);
        System.out.println("Czas to: " + (roznicaCzasu.getSeconds() / 60));
    }

    public void usunZRejestru(String nrRejestracyjny) {
        rejestr.remove(nrRejestracyjny);
    }
}
