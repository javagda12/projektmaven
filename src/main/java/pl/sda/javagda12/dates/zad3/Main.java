package pl.sda.javagda12.dates.zad3;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Main {
    public static void main(String[] args) {
        LocalDateTime date = LocalDateTime.of(1970, 1, 1, 00, 00, 00, 00);

        LocalDateTime now = LocalDateTime.now();

//        Period period = Period.between(date, now);
        Duration duration = Duration.between(date, now);


//        System.out.println("Years: " + period.getYears() + ", months: " + period.getMonths() + ", days: " + period.getDays());

        Long seconds = duration.getSeconds();
        System.out.println(seconds);
        System.out.println(System.currentTimeMillis());
    }
}
