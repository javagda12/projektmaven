package pl.sda.javagda12.dates.zad1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String linia;
        do {
            linia = scanner.nextLine();

            if (linia.equalsIgnoreCase("date")) {
                LocalDate date = LocalDate.now();

//                System.out.println(date);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");

                System.out.println(date.format(formatter));
            } else if (linia.equalsIgnoreCase("time")) {
                LocalTime time = LocalTime.now();

//                System.out.println(time);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

                System.out.println(time.format(formatter));
            } else if (linia.equalsIgnoreCase("datetime")) {
                LocalDateTime datetime = LocalDateTime.now();

//                System.out.println(time);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd-HH:mm");

                System.out.println(datetime.format(formatter));
            }

        } while (!linia.equalsIgnoreCase("quit"));
    }
}
