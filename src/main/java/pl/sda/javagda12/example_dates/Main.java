package pl.sda.javagda12.example_dates;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        // otrzymywanie daty używając 'now'
        LocalDate date = LocalDate.now();
        LocalDate dateOf = LocalDate.of(2000, 01, 01);
        LocalDateTime dateTime = LocalDateTime.of(2000, 01, 01, 01, 01, 01, 01);

        // formatowanie daty (data -> string)
        System.out.println(dateTime);
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("'['yyyy-MM-dd']'");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String sformatowanaData = dateTime.format(formatter);
        System.out.println(sformatowanaData);

        // otrzymywanie daty ( string -> data)
//        String dataWFormacie = "[2013-03-03]";
        String dataWFormacie = "2013-03-03";
        LocalDate dateTimeZ = LocalDate.parse(dataWFormacie, formatter);
        System.out.println(dateTimeZ);

        // duration do różnicy czasu (godziny/dni)
        Duration duration = Duration.between(dateTime, LocalDateTime.now());

        // period miesiące/lata/dni
        Period period = Period.between(date, LocalDate.now());
        System.out.println(period.getYears() + " " + period.getDays());

        // wypisanie timestamp
        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis());
        System.out.println("Cokolwiek wypisane");

        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis());
        System.out.println(System.currentTimeMillis());

        System.out.println(UUID.randomUUID().toString());
        ;

    }
}
